/**
 * Bootstrap
 * (sails.config.bootstrap)
 *
 * An asynchronous bootstrap function that runs before your Sails app gets lifted.
 * This gives you an opportunity to set up your data model, run jobs, or perform some special logic.
 *
 * For more information on bootstrapping your app, check out:
 * http://sailsjs.org/#!/documentation/reference/sails.config/sails.config.bootstrap.html
 */

module.exports.bootstrap = function(cb) {

  // It's very important to trigger this callback method when you are finished
  // with the bootstrap!  (otherwise your server will never lift, since it's waiting on the bootstrap)

	var net = require('net');
	var server = net.createServer(function(c) { //'connection' listener
		sails.log.info('client connected');

		c.on('end', function() {
			sails.log.info('client disconnected');
		});

		c.on('data', function(data) {
			var msg = data.toString();
			ParsingService.parser(msg, function(err, result){
				if(err)
					sails.log.error(err);
				c.write(result);
			});
		});

		c.pipe(c);
	});

	server.listen(8124, function() { //'listening' listener
		sails.log.info('server bound');
	});

  	cb();
};
