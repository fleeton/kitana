var MongoClient = require('mongodb').MongoClient;
var test = require('assert');

// Connection url
var url = 'mongodb://nkman:nkmannkman@oceanic.mongohq.com:10057/orderchhotu';
var _db; //global connection variable;

initMongoConnection = function(cb){
	MongoClient.connect(url, function(err, db) {
		_db = db;
		return cb(err);
	});
}

exports.getDb = function(){
	return _db;
}

initMongoConnection(function(err){
	if(err)
		sails.log.error(err);
	else
		sails.log.info("New mongodb connection created!");
})