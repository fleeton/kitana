exports.parser = function(msg, cb){
	/*
	   (TLN,356917050291991,090315,133525,+12.990582,-77.589080,0,0,944,13,1,5,000,00,0.00,10.88,6.31,29.55,0.00,0,0.99,66,0,0,88,95);
	   */
	var packetType = msg.substring(1,4);
	/* Bulk not implemented */

	switch(packetType){
		case 'TLN':
			TLNpacketExtractor(msg, function(result){
				return cb(null, result);
			});
			break;

		case 'TLA':
			TLApacketExtractor(msg, function(result){
				return cb(null, result);
			});
			break;

		case 'TLB':
			TLBpacketExtractor(msg, function(result){
				return cb(null, result);
			});
			break;

		case 'TLL':
			TLLpacketExtractor(msg, function(result){
				return cb(null, result);
			});
			break;
	}
	return cb(null, packetType);
}

TLNpacketExtractor = function(msg, cb){
	var exMsg = msg.split(',');
	var imei = exMsg[1];

	try {
		var utcDate = parseInt(exMsg[2]);
		var utcTime = parseInt(exMsg[3]);
		var speed = parseInt(exMsg[6]);
		var courseOverGround = parseInt(exMsg[7]);
		var altitude = parseInt(exMsg[8]);
		var noSatellite = parseInt(exMsg[9]);
		var gpsStatus = parseInt(exMsg[10]);
		var gsmSignalstrength = parseInt(exMsg[11]);
		var digitalInputStatus = parseInt(exMsg[12]);
		var digitalOutputStatus = parseInt(exMsg[13]);
		var trip = parseInt(exMsg[22]);
		var packetSerialNumber = parseInt(exMsg[24]);
		var MovingStatus = parseInt(exMsg[19]);

		var lat = parseFloat(exMsg[4]);
		var lang = parseFloat(exMsg[5]);
		var analogInputValue = parseFloat(exMsg[14]);
		var ExternalBattery = parseFloat(exMsg[15]);
		var internalBattery = parseFloat(exMsg[16]);
		var pcbTemperature = parseFloat(exMsg[17]);
		var oneWireData = parseFloat(exMsg[18]);
		var accelGValue = parseFloat(exMsg[20]);
		var tiltValue = parseFloat(exMsg[21]);
		var checkSum = parseFloat(exMsg[25]);
		var virtualOdometer = parseFloat(exMsg[23]);
	}
	catch(err) {
		sails.log.error(err);
		return cb("error");
	}

	var db = MongoConnectionService.getDb();
	Tracker.findOne({imei: imei}, {vehicleId:1}).exec(function(err, vehicle){
		if(err){
			sails.log.error(err);
			return cb("error"); //Modify this line. This is not the standard output.
		}

		if(!vehicle){
			sails.log.info("Unknown tracker "+imei+" sent a request!");
			return cb("error");
		}

		var vehicleId = vehicle.vehicleId;
		var vehicleCollection = db.collection('vehicle_'+vehicleId);
		if(!vehicleCollection) {
			//Not sure if this line's correct.
			vehicleCollection = db.createCollection('vehicle_'+vehicleId);
		}

		vehicleCollection.insert({imei: imei, utcDate: utcDate, utcTime: utcTime, lat: lat, lang: lang, speed: speed, 
			courseOverGround: courseOverGround, altitude: altitude, noSatellite: noSatellite, gpsStatus: gpsStatus, 
			gsmSignalstrength: gsmSignalstrength, digitalInputStatus: digitalInputStatus, digitalOutputStatus: digitalOutputStatus,
			analogInputValue: analogInputValue, ExternalBattery: ExternalBattery, internalBattery: internalBattery, 
			pcbTemperature: pcbTemperature, oneWireData: oneWireData, MovingStatus: MovingStatus, accelGValue: accelGValue, 
			tiltValue: tiltValue, trip: trip, virtualOdometer: virtualOdometer, packetSerialNumber: packetSerialNumber, checkSum: checkSum}, 
			{w: 1}, 

			function(err, savedVehicle){
				if(err){
					sails.log.error(err);
					return cb("error"); //TODO: Change this
				}

				return cb("okay");	//TODO: Change this
			});
	});
}

/*(TLA,356917050217335,190115,011336,+12.932403,+79.898887,0,0,71.7,08,3,10,000,00,0.00,10.41,7.07,26.84,0.00,0,0.99,63, 0,0,0,0,0,0,000000000,0,0,0,0,0,0,0,2,0,0,14,86);*/
TLApacketExtractor = function(msg, cb){
	var exMsg = msg.split(',');


	try{
		var imei = exMsg[1];
		var digitalInputStatus = exMsg[12];
		var digitalOutputStatus = exMsg[13];
		var individualGeofenceAlert = exMsg[28];
		var rfid = exMsg[37];

		var utcDate = parseInt(exMsg[2]);
		var utcTime = parseInt(exMsg[3]);
		var speed = parseInt(exMsg[6]);
		var courseOverGround = parseInt(exMsg[7]);
		var altitude = parseInt(exMsg[8]);
		var noSatellite = parseInt(exMsg[9]);
		var gpsStatus = parseInt(exMsg[10]);
		var gsmSignalstrength = parseInt(exMsg[11]);
		var MovingStatus = parseInt(exMsg[19]);
		var tiltValue = parseInt(exMsg[21]);
		var trip = parseInt(exMsg[22]);
		var virtualOdometer = parseInt(exMsg[23]);
		var overSpeedAlert = parseInt(exMsg[24]);
		var input2MisuseAlert = parseInt(exMsg[25]);
		var immobilizerAlert = parseInt(exMsg[26]);
		var extTemperatureAlert = parseInt(exMsg[27]);
		var ecoDrivingAlert =parseInt(exMsg[29]);
		var deviceReconnectAlert = parseInt(exMsg[30]);
		var internalBatteryLowAlert = parseInt(exMsg[31]);
		var extPowerSrcRemovedAlert = parseInt(exMsg[32]);
		var gpsModuleFailureAlert = parseInt(exMsg[33]);
		var towAwayAlert = parseInt(exMsg[34]);
		var serverNotReachable = parseInt(exMsg[35]);
		var sleepMode = parseInt(exMsg[36]);
		var smsCount = parseInt(exMsg[38]);
		var packetSerialNum = parseInt(exMsg[39]);
		var checksum = parseInt(exMsg[40]);

		var lat = parseFloat(exMsg[4]);
		var lang = parseFloat(exMsg[5]);
		var analogInputValue = parseFloat(exMsg[14]);
		var ExternalBattery = parseFloat(exMsg[15]);
		var internalBattery = parseFloat(exMsg[16]);
		var pcbTemperature = parseFloat(exMsg[17]);
		var oneWireData = parseFloat(exMsg[18]);
		var accelGValue = parseFloat(exMsg[20]);

	}
	catch(err) {
		sails.log.error(err);
		return cb("error");
	}

	var db = MongoConnectionService.getDb();
	Tracker.findOne({imei: imei}, {vehicleId:1}).exec(function(err, vehicle){
		if(err){
			sails.log.error(err);
			return cb("error"); //Modify this line. This is not the standard output.
		}

		if(!vehicle){
			sails.log.info("Unknown tracker "+imei+" sent a request!");
			return cb("error");
		}

		var vehicleId = vehicle.vehicleId;
		var vehicleCollection = db.collection('vehicle_'+vehicleId);
		if(!vehicleCollection) {
			//Not sure if this line's correct.
			vehicleCollection = db.createCollection('vehicle_'+vehicleId);
		}
		vehicleCollection.insert({imei: imei, utcDate: utcDate, utcTime: utcTime, lat: lat, lang: lang, speed: speed, 
			courseOverGround: courseOverGround, altitude: altitude, noSatellite: noSatellite, gpsStatus: gpsStatus, 
			gsmSignalstrength: gsmSignalstrength, digitalInputStatus: digitalInputStatus, digitalOutputStatus: digitalOutputStatus,
			analogInputValue: analogInputValue, ExternalBattery: ExternalBattery, internalBattery: internalBattery, 
			pcbTemperature: pcbTemperature, oneWireData: oneWireData, MovingStatus: MovingStatus, accelGValue: accelGValue, 
			tiltValue: tiltValue, trip: trip, virtualOdometer: virtualOdometer, overSpeedAlert: overSpeedAlert, input2MisuseAlert: input2MisuseAlert, immobilizerAlert:immobilizerAlert, extTemperatureAlert: extTemperatureAlert, individualGeofenceAlert: individualGeofenceAlert, ecoDrivingAlert: ecoDrivingAlert, deviceReconnectAlert: deviceReconnectAlert, internalBatteryLowAlert: internalBatteryLowAlert, extPowerSrcRemovedAlert: extPowerSrcRemovedAlert, gpsModuleFailureAlert: gpsModuleFailureAlert, towAwayAlert: towAwayAlert, serverNotReachable: serverNotReachable, sleepMode: sleepMode, rfid: rfid, smsCount: smsCount, packetSerialNum: packetSerialNum, checksum: checksum}, 
			{w: 1}, 

			function(err, savedVehicle){
				if(err){
					sails.log.error(err);
					return cb("error"); //TODO: Change this
				}

				return cb("okay");	//TODO: Change this
			});

	});
}

TLBpacketExtractor = function(msg, cb){
	var exMsg = msg.split(',');
	var imei = exMsg[1];

	try {
		var utcDate = parseInt(exMsg[2]);
		var utcTime = parseInt(exMsg[3]);
		var speed = parseInt(exMsg[6]);
		var courseOverGround = parseInt(exMsg[7]);
		var altitude = parseInt(exMsg[8]);
		var noSatellite = parseInt(exMsg[9]);
		var gpsStatus = parseInt(exMsg[10]);
		var gsmSignalstrength = parseInt(exMsg[11]);
		var digitalInputStatus = parseInt(exMsg[12]);
		var digitalOutputStatus = parseInt(exMsg[13]);
		var trip = parseInt(exMsg[22]);
		var packetSerialNumber = parseInt(exMsg[24]);
		var MovingStatus = parseInt(exMsg[19]);

		var lat = parseFloat(exMsg[4]);
		var lang = parseFloat(exMsg[5]);
		var analogInputValue = parseFloat(exMsg[14]);
		var ExternalBattery = parseFloat(exMsg[15]);
		var internalBattery = parseFloat(exMsg[16]);
		var pcbTemperature = parseFloat(exMsg[17]);
		var oneWireData = parseFloat(exMsg[18]);
		var accelGValue = parseFloat(exMsg[20]);
		var tiltValue = parseFloat(exMsg[21]);
		var checkSum = parseFloat(exMsg[25]);
		var virtualOdometer = parseFloat(exMsg[23]);
	}
	catch(err) {
		sails.log.error(err);
		return cb("error");
	}

	var db = MongoConnectionService.getDb();
	Tracker.findOne({imei: imei}, {vehicleId:1}).exec(function(err, vehicle){
		if(err){
			sails.log.error(err);
			return cb("error"); //Modify this line. This is not the standard output.
		}

		if(!vehicle){
			sails.log.info("Unknown tracker "+imei+" sent a request!");
			return cb("error");
		}

		var vehicleId = vehicle.vehicleId;
		var vehicleCollection = db.collection('vehicle_'+vehicleId);
		if(!vehicleCollection) {
			//Not sure if this line's correct.
			vehicleCollection = db.createCollection('vehicle_'+vehicleId);
		}

		vehicleCollection.insert({imei: imei, utcDate: utcDate, utcTime: utcTime, lat: lat, lang: lang, speed: speed, 
			courseOverGround: courseOverGround, altitude: altitude, noSatellite: noSatellite, gpsStatus: gpsStatus, 
			gsmSignalstrength: gsmSignalstrength, digitalInputStatus: digitalInputStatus, digitalOutputStatus: digitalOutputStatus,
			analogInputValue: analogInputValue, ExternalBattery: ExternalBattery, internalBattery: internalBattery, 
			pcbTemperature: pcbTemperature, oneWireData: oneWireData, MovingStatus: MovingStatus, accelGValue: accelGValue, 
			tiltValue: tiltValue, trip: trip, virtualOdometer: virtualOdometer, packetSerialNumber: packetSerialNumber, checkSum: checkSum}, 
			{w: 1}, 

			function(err, savedVehicle){
				if(err){
					sails.log.error(err);
					return cb("error"); //TODO: Change this
				}

				return cb("okay");	//TODO: Change this
			});
	});
}

TLLpacketExtractor = function(msg, cb){
	var exMsg = msg.split(',');


	try{
		var imei = exMsg[1];
		var digitalInputStatus = exMsg[12];
		var digitalOutputStatus = exMsg[13];
		var individualGeofenceAlert = exMsg[28];
		var rfid = exMsg[37];

		var utcDate = parseInt(exMsg[2]);
		var utcTime = parseInt(exMsg[3]);
		var speed = parseInt(exMsg[6]);
		var courseOverGround = parseInt(exMsg[7]);
		var altitude = parseInt(exMsg[8]);
		var noSatellite = parseInt(exMsg[9]);
		var gpsStatus = parseInt(exMsg[10]);
		var gsmSignalstrength = parseInt(exMsg[11]);
		var MovingStatus = parseInt(exMsg[19]);
		var tiltValue = parseInt(exMsg[21]);
		var trip = parseInt(exMsg[22]);
		var virtualOdometer = parseInt(exMsg[23]);
		var overSpeedAlert = parseInt(exMsg[24]);
		var input2MisuseAlert = parseInt(exMsg[25]);
		var immobilizerAlert = parseInt(exMsg[26]);
		var extTemperatureAlert = parseInt(exMsg[27]);
		var ecoDrivingAlert =parseInt(exMsg[29]);
		var deviceReconnectAlert = parseInt(exMsg[30]);
		var internalBatteryLowAlert = parseInt(exMsg[31]);
		var extPowerSrcRemovedAlert = parseInt(exMsg[32]);
		var gpsModuleFailureAlert = parseInt(exMsg[33]);
		var towAwayAlert = parseInt(exMsg[34]);
		var serverNotReachable = parseInt(exMsg[35]);
		var sleepMode = parseInt(exMsg[36]);
		var smsCount = parseInt(exMsg[38]);
		var packetSerialNum = parseInt(exMsg[39]);
		var checksum = parseInt(exMsg[40]);

		var lat = parseFloat(exMsg[4]);
		var lang = parseFloat(exMsg[5]);
		var analogInputValue = parseFloat(exMsg[14]);
		var ExternalBattery = parseFloat(exMsg[15]);
		var internalBattery = parseFloat(exMsg[16]);
		var pcbTemperature = parseFloat(exMsg[17]);
		var oneWireData = parseFloat(exMsg[18]);
		var accelGValue = parseFloat(exMsg[20]);

	}
	catch(err) {
		sails.log.error(err);
		return cb("error");
	}

	var db = MongoConnectionService.getDb();
	Tracker.findOne({imei: imei}, {vehicleId:1}).exec(function(err, vehicle){
		if(err){
			sails.log.error(err);
			return cb("error"); //Modify this line. This is not the standard output.
		}

		if(!vehicle){
			sails.log.info("Unknown tracker "+imei+" sent a request!");
			return cb("error");
		}

		var vehicleId = vehicle.vehicleId;
		var vehicleCollection = db.collection('vehicle_'+vehicleId);
		if(!vehicleCollection) {
			//Not sure if this line's correct.
			vehicleCollection = db.createCollection('vehicle_'+vehicleId);
		}
		vehicleCollection.insert({imei: imei, utcDate: utcDate, utcTime: utcTime, lat: lat, lang: lang, speed: speed, 
			courseOverGround: courseOverGround, altitude: altitude, noSatellite: noSatellite, gpsStatus: gpsStatus, 
			gsmSignalstrength: gsmSignalstrength, digitalInputStatus: digitalInputStatus, digitalOutputStatus: digitalOutputStatus,
			analogInputValue: analogInputValue, ExternalBattery: ExternalBattery, internalBattery: internalBattery, 
			pcbTemperature: pcbTemperature, oneWireData: oneWireData, MovingStatus: MovingStatus, accelGValue: accelGValue, 
			tiltValue: tiltValue, trip: trip, virtualOdometer: virtualOdometer, overSpeedAlert: overSpeedAlert, input2MisuseAlert: input2MisuseAlert, immobilizerAlert:immobilizerAlert, extTemperatureAlert: extTemperatureAlert, individualGeofenceAlert: individualGeofenceAlert, ecoDrivingAlert: ecoDrivingAlert, deviceReconnectAlert: deviceReconnectAlert, internalBatteryLowAlert: internalBatteryLowAlert, extPowerSrcRemovedAlert: extPowerSrcRemovedAlert, gpsModuleFailureAlert: gpsModuleFailureAlert, towAwayAlert: towAwayAlert, serverNotReachable: serverNotReachable, sleepMode: sleepMode, rfid: rfid, smsCount: smsCount, packetSerialNum: packetSerialNum, checksum: checksum}, 
			{w: 1}, 

			function(err, savedVehicle){
				if(err){
					sails.log.error(err);
					return cb("error"); //TODO: Change this
				}

				return cb("okay");	//TODO: Change this
			});

	});
}

