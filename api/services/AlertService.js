var overSpeed = 0;
var geoFence = 1;
var ignition = 2;
var powercut = 3; //-->???
var harshDrive = 4; // how to get this?


/*
 * fields is the interested fields after parsing the incoming vehicle data in a json variable. 
 * ex. {lat: lat, long: long, speed: speed, etc: etc};
 */

exports.checkAlerts = function(vehicleId, fields, cb){
    Alert.find({vehicleId: vehicleId, isActive: true}).exec(function(err, alerts){
        if(err){
            sails.log.error("Unable to get alert details for vehicleId="+vehicleId+" DB Error occured!");
            sails.log.error(err);
            return cb(err, null);
        }

        if(!alerts){
            return cb(null, true);
        }

        for(var i=0; i<alerts.length; i++){
            (function(alert, fields){
                checkAlertPerDevice(alert, fields, function(err, notice){
                    if(err){
                        sails.log.error("Failed to access alerts of vehicleId="+alert.vehicleId+" and usedId="+userId);
                        sails.log.error(err);
                    }

                    //else true
                });
            })(alerts[i], fields);
        }
    });
}

var checkAlertPerDevice = function(alert, fields, cb){
    var actions = [];
    for(var i=0; i<alert.action.length; i++){
        if(alert.action[i] == "1"){
            actions.push(i);
        }
    }

    for(var i=0; i<actions.length; i++){
        switch(actions[i]){
        case overSpeed:
            (function(id, userId, vehicleId, maxSpeed, maxSpeedAchieved, alertBy, fields){
                calculateOverSpeed(id, userId, vehicleId, maxSpeed, maxSpeedAchieved, alertBy, fields);
            })(alert.id, alert.userId, alert.vehicleId, alert.maxSpeed, alert.maxSpeedAchieved, alert.alert, fields);
            break;
        case geoFence: 

            break;
        case ignition:

            break;
        case checkPowerCut:
            (function(id, userId, vehicleId, fields){
                      checkPowerCut(id, userId, vehicleId, fields)      
            })(actions[i].id, actions[i].userId, actions[i].vehicleId, fields)
            break;
        case harshDrive:
            (function(userId, vehicleId, alertBy, fields){
                checkHarshDrive(userId, vehicleId, alertBy, fields);
            })(alert.userId, alert.vehicleId, alert.alert, fields);
            break;
        }
    }
}

var calculateOverSpeed = function(id, userId, vehicleId, maxSpeed, maxSpeedAchieved, alertBy, fields){
    if(maxSpeedAchieved){
        if(fields.speed <= maxSpeed){
            Alert.update({id: id}, {maxSpeedAchieved: false}).exec(function(err, updatedAlert){
                if(err){
                    sails.log.error("unable to update the alert!");
                    sails.log.error(err);
                }
                return;
            });
        }
        //else --> still in high speed.
    }

    else{
        if(fields.speed > maxSpeed){
            Alert.update({id: id}, {maxSpeedAchieved: true}).exec(function(err, updatedAlert){
                if(err){
                    sails.log.error("unable to update the alert!");
                    sails.log.error(err);
                    return;
                }

                Vehicle.findOne({id: vehicleId}).exec(function(err, vehicle){
                    if(err){
                        sails.log.error(err);
                        return;
                    }

                    var message = "Vehicle "+vehicle.regNum+" has found to be exceeding the speed limit of "+maxSpeed.toString()+" by running at "+fields.speed.toString();

                    sendAlert(userId, id, message, overSpeed, alertBy[overSpeed]);
                    return;
                });
            });
        }

        //else --> Everything is cool!!
    }
}

var checkHarshDrive = function(userId, vehicleId, alertBy, fields){
    if(!fields.ecoDrivingAlert)
        return;
    else

    Vehicle.findOne({id: vehicleId}).exec(function(err, vehicle){
        if(err){
            sails.log.error("Database error occured!");
            sails.log.error(err);
            return;
        }

        if(!vehicle){
            sails.log.info("Obsolate vehicle data present in database.")
            return;
        }

        switch(ecoDrivingAlert){
        case 1:
            var message = "Harsh acceleration reported for "+vehicle.regNum+" at "+fields.reportedTime;
            break;
        case 2:
            var message = "Harsh braking reported for "+vehicle.regNum+" at "+fields.reportedTime;
            break;
        case 3:
            var message = "Harsh cornering reported for "+vehicle.regNum+" at "+fields.reportedTime;
            break;
        default:
            return;
        }
        
        sendAlert(userId, alertId, message, harshDrive, alertBy[harshDrive]);
    });
}


/* 
 * Saving notification in table
 */
var saveNotification = function(userId, alertId, message, msgCode){
    Notification.create({userId: userId, alertId: alertId, message: message, msgCode: msgCode}).exec(function(err, notice){
        if(err){
            sails.log.error(err);
            return;
        }

        return;
    });
}


var sendAlert = function(userId, alertId, message, msgCode, flag){
    flag = parseInt(flag);
    if(flag == 0)
        return;

    var binary = (flag>>>0).toString(2);

    if(parseInt(binary[0]))
        saveNotification(userId, alertId, message, msgCode);
    if(parseInt(binary[1])){
        //Email
    }
    if(parseInt(binary[2])){
        //sms
    }
}

var checkPowerCut = function(id, userId, vehicleId, fields){
    if(fields.extPowerCut)
    {
        //send alert
    }
}
