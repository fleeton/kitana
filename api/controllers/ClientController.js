/**
 * ClientController
 *
 * @description :: Server-side logic for managing clients
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

module.exports = {
	test: function(req, res){

		var f = Client.find({}).exec(function(a, err){
			if(err)
				return res.send(err, 500);
			return res.send(a, 200);
		})
	}
};

