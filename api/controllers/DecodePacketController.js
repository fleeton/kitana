/**
 * DecodePacketController
 *
 * @description :: Server-side logic for managing Decodepackets
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

module.exports = {
	parser: function(req, res){
		sails.log("TIME: "+ new Date()+ ": "
			+req.method+ " " +req.headers.host+" " +req.url+ " "
			+JSON.stringify(req.params)+" "+req.route.params
		);

		return res.send("Running peacefully!!");
	}
};

