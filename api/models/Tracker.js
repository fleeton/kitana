/**
* Tracker.js
*
* @description :: TODO: You might write a short summary of how this model works and what it represents here.
* @docs        :: http://sailsjs.org/#!documentation/models
*/

module.exports = {

  attributes: {
	phone: {
		type: 'STRING',
		required: true
	},

	imei: {
		type: 'STRING',
		required: true
	},

	vehicleId: {
		type: 'STRING',
		required: true
	}
  }
};

