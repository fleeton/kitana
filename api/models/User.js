/**
* User.js
*
* @description :: User: all the details related to a user. TODO: Add bcrypt encryption on password.
* @docs        :: http://sailsjs.org/#!documentation/models
*/

module.exports = {

  attributes: {
  	username: {
  		type: 'STRING',
  		required: true,
		unique: true
	},
  	password: {
  		type: 'STRING',
  		minLength: 4,
  		required: true
  	},
  	clientId: {
  		type: 'STRING',
  		required: true
  	},
  	expireDate: {
  		type: 'datetime'
  	},
  	email: {
  		type: 'STRING',
  		required: true
  	},
  	phone: {
  		type: 'STRING',
  		required: true
  	},

  	isPhoneVerified: {
  		type: 'BOOLEAN',
  		defaultsTo: false
  	},
  	isEmailVerified: {
  		type: 'BOOLEAN',
  		defaultsTo: false
  	}
  }
};

