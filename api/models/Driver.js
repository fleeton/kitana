/**
* Driver.js
*
* @description :: TODO: You might write a short summary of how this model works and what it represents here.
* @docs        :: http://sailsjs.org/#!documentation/models
*/

module.exports = {

  attributes: {
  	name: {
  		type: 'STRING',
  		required: true
  	},
  	address: {
  		type: 'STRING'
  	},
  	phone: {
  		type: 'STRING',
  		required: true
  	},
  	description: {
  		type: 'STRING'
  	},
  	addedBy: {
  		/* Added by which client */
  		type: 'STRING',
  		required: true
  	}
  }
};

