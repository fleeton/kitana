/**
* Alert.js
*
* @description :: Alerts schema
* expiryTime - Not using this for now.
* maxSpeed - If overspeed alerts has been set, then this will be the limit.
* maxSpeedAchieved - If speed transition from low->high then this flag = true, If speed is < maxSpeed, this flag = false
* geoFenceIds - All the geofences active for this vehicle. ex - ["geofenceId1", "geofenceid2", "geofenceId3",....]
* isActive - Flag to deactivate this alert 
* @docs        :: http://sailsjs.org/#!documentation/models
*/

module.exports = {

  attributes: {

    userId: {
        type: 'STRING',
        required: true
    },

    vahicleId: {
        type: 'STRING',
        required: true
    },

    action: {
        type: 'STRING',
        defaultsTo: "00000",
        minLength: 5,
        maxLength: 5
    },

    alert: {
        type: 'STRING',
        defaultsTo: "00000",
        minLength: 5,
        maxLength: 5
    },

    expiryTime: {
        type: 'datetime'
    },

    maxSpeed: {
        type: 'INTEGER'
    },

    maxSpeedAchieved: {
        type: 'BOOLEAN',
        defaultsTo: false
    },

    geoFenceIds: {
        type: 'ARRAY',
        required: false
    },

    isActive: {
        type: 'BOOLEAN',
        defaultsTo: false
    }
  }
};

