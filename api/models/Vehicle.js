/**
* Vehicle.js
*
* @description :: TODO: You might write a short summary of how this model works and what it represents here.
* Type - from collection vehicle-type
* regNum - Registration Number 
* addedBy - added by which client Id
* @docs        :: http://sailsjs.org/#!documentation/models
*/

module.exports = {

  attributes: {
  	vehicleType: {
  		type: 'INTEGER',
  		required: true
  	},

    regNum: {
      type: 'STRING',
      required: true
    },

  	addedBy: {
  		type: 'STRING',
  		required: true
  	}
  }
};

