/**
* Client.js
*
* @description :: TODO: You might write a short summary of how this model works and what it represents here.
* @docs        :: http://sailsjs.org/#!documentation/models
*/

module.exports = {

  attributes: {
  	name: {
  		type: 'STRING',
  		required: true
  	},
  	masterUserId: {
  		type: 'STRING',
  		required: true
  	},
  	description: 'STRING',
  	isActive: {
  		type: 'INTEGER',
  		defaultsTo: 1
  	},
  	phone: {
  		type: 'STRING',
  		required: true
  	}
  }
};

