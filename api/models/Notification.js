/**
* Notification.js
*
* @description :: TODO: You might write a short summary of how this model works and what it represents here.
* @docs        :: http://sailsjs.org/#!documentation/models
*/

module.exports = {

  attributes: {
    userId: {
        type: 'STRING',
        required: true
    },

    alertId: {
        type: 'STRING',
        required: true
    },

    message: {
        type: 'STRING',
        required: true
    },

    msgCode: {
        type: 'INTEGER',
        required: true
    },

    seen: {
        type: 'BOOLEAN',
        defaultsTo: false
    }
  }
};

