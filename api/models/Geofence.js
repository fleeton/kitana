/**
* Geofence.js
*
* @description :: Represent single geofence which can be used in multiple alerts.
* @docs        :: http://sailsjs.org/#!documentation/models
*/

module.exports = {

  attributes: {

    userId: {
        type: 'STRING',
        required: true
    },

    name: {
        type: 'STRING',
        required: true
    },

    type: {
        type: 'STRING',
        enum: ['circle', 'polygon', 'triangle', 'rectangle'],
        required: true
    },

    points: {
        type: 'ARRAY',
        required: true
    },

    radius: {
        type: 'INTEGER',
        required: false
    }
  }
};

